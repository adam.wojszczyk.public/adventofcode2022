import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class Day_2 {

	public static void main(String[] args) throws URISyntaxException, IOException {
		List<String> lines = Files.lines(Path.of(Day_2.class.getResource("Day_2.txt").toURI())).toList();

		int score1 = lines.stream().map(Game1::new).mapToInt(Game1::points).sum();
		System.out.println(score1);

		int score2 = lines.stream().map(Game2::new).mapToInt(Game2::points).sum();
		System.out.println(score2);
	}

	static class Game1 {

		private final Choice my;

		private final Choice oponent;

		Game1(String game) {
			this(game.split(" "));
		}

		public Game1(String[] s) {
			this(s[0], s[1]);
		}

		public Game1(String oponent, String my) {
			this(Choice.parse(my), Choice.parse(oponent));
		}

		Game1(Choice my, Choice oponent) {
			this.my = my;
			this.oponent = oponent;
		}

		int points() {
			return my.points() + my.compete(oponent).points();
		}
	}

	static class Game2 {

		private final Choice my;

		private final Choice oponent;

		Game2(String game) {
			this(game.split(" "));
		}

		public Game2(String[] s) {
			this(s[0], s[1]);
		}

		public Game2(String oponent, String expectedResult) {
			this(Choice.parse(oponent), Result.parse(expectedResult));
		}

		Game2(Choice oponent, Result expectedResult) {
			this(
				switch (expectedResult) {
					case DRAW -> oponent.draw();
					case LOSE -> oponent.winsWith();
					case WIN -> oponent.losesWith();
				},
				oponent
			);
		}

		Game2(Choice my, Choice oponent) {
			this.my = my;
			this.oponent = oponent;
		}

		int points() {
			return my.points() + my.compete(oponent).points();
		}
	}

	enum Choice {
		ROCK(1), PAPER(2), SCISSORS(3);

		private final int points;

		Choice(int points) {
			this.points = points;
		}

		static Choice parse(String choice) {
			return switch (choice) {
				case "A", "X" -> ROCK;
				case "B", "Y" -> PAPER;
				case "C", "Z" -> SCISSORS;
				default -> throw new IllegalArgumentException(choice + " is unknown");
			};
		}

		int points() {
			return points;
		}

		Result compete(Choice opponent) {
			if (this.equals(opponent)) return Result.DRAW;
			return switch (this) {
				case ROCK -> opponent == SCISSORS ? Result.WIN : Result.LOSE;
				case PAPER -> opponent == ROCK ? Result.WIN : Result.LOSE;
				case SCISSORS -> opponent == PAPER ? Result.WIN : Result.LOSE;
			};
		}

		Choice draw() {
			return this;
		}

		Choice losesWith() {
			return switch (this) {
				case ROCK -> PAPER;
				case SCISSORS -> ROCK;
				case PAPER -> SCISSORS;
			};
		}

		Choice winsWith() {
			return switch (this) {
				case ROCK -> SCISSORS;
				case SCISSORS -> PAPER;
				case PAPER -> ROCK;
			};
		}
	}

	enum Result {
		WIN(6), DRAW(3), LOSE(0);

		private final int points;

		Result(int points) {
			this.points = points;
		}

		static Result parse(String result) {
			return switch (result) {
				case "Y" -> DRAW;
				case "X" -> LOSE;
				case "Z" -> WIN;
				default -> throw new IllegalArgumentException(result + " is unknown");
			};
		}

		int points() {
			return points;
		}
	}
}
