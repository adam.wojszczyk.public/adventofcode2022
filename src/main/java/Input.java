import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public interface Input {

	static List<List<String>> groupLines(int size, List<String> lines) {
		List<List<String>> groups = new ArrayList<>();
		int groupsCount = lines.size() / size;
		for (int groupIndex = 0; groupIndex < groupsCount; groupIndex++) {
			List<String> group = lines.subList(0 + (groupIndex * size), size + (groupIndex * size));
			groups.add(group);
		}
		return groups;
	}

	static List<String> lines(String filename) {
		try {
			return Files.lines(Path.of(Input.class.getResource(filename).toURI())).toList();
		} catch (IOException | URISyntaxException e) {
			throw new RuntimeException(e);
		}
	}
}