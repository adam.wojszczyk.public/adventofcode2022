import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day_9 {

	public static void main(String[] args) {
		List<String> lines = Input.lines("Day_9.txt");
		List<Move> moves = lines.stream().map(Move::parse).toList();
		Rope shortRope = new Rope(2);
		moves.forEach(shortRope::move);
		int shortRopeTailVisits = new HashSet<>(shortRope.tailHistory()).size();
		System.out.println(shortRopeTailVisits);

		Rope longRope = new Rope(10);
		moves.forEach(longRope::move);
		int longRopeTailVisits = new HashSet<>(longRope.tailHistory()).size();
		System.out.println(longRopeTailVisits);
	}

	private static class Rope {

		private final Knot head;

		private Rope(int length) {
			this.head = new Knot();
			Knot previous = head;
			int lenghtMinusHead = length - 1;
			for (int i = 0; i < lenghtMinusHead; i++) {
				previous = new Knot(previous);
			}
		}

		public void move(Move move) {
			int distance = move.distance();
			for (int i = 0; i < distance; i++) {
				head.move(move.direction());
			}
		}

		public List<Position> tailHistory() {
			return head.tail().history();
		}

		private static class Knot {

			private Position current = Position.center();
			private final List<Position> previous = new ArrayList<>();
			private Knot follower;

			Knot() {
				this(null);
			}

			Knot(Knot followed) {
				previous.add(current);
				if (followed != null) {
					followed.setFollower(this);
				}
			}

			private void setFollower(Knot follower) {
				this.follower = follower;
			}

			public void move(Direction direction) {
				moveTo(current.adjacentInDirection(direction));
			}

			public void moveTo(Position next) {
				previous.add(current);
				current = next;
				if (hasFollower()) {
					if (current.distanceTo(follower.current) > 1) {
						Direction direction = current.inDirectionFrom(follower.current);
						follower.move(direction);
					}
				}
			}

			public List<Position> history() {
				List<Position> history = previous;
				history.add(current);
				return history;
			}

			public Knot tail() {
				if (follower.isTail()) {
					return follower;
				} else {
					return follower.tail();
				}
			}

			private boolean hasFollower() {
				return !isTail();
			}

			public boolean isTail() {
				return follower == null;
			}
		}

		private record Position(int x, int y) {

			static Position center() {
				return new Position(0, 0);
			}

			int distanceTo(Position o) {
				return Math.max(Math.abs(x - o.x), Math.abs(y - o.y));
			}

			Position adjacentInDirection(Direction direction) {
				return switch (direction) {
					case N -> new Position(x, y + 1);
					case S -> new Position(x, y - 1);
					case W -> new Position(x - 1, y);
					case E -> new Position(x + 1, y);
					case NW -> new Position(x - 1, y + 1);
					case NE -> new Position(x + 1, y + 1);
					case SW -> new Position(x - 1, y - 1);
					case SE -> new Position(x + 1, y - 1);
				};
			}

			Direction inDirectionFrom(Position o) {
				if (x == o.x && y > o.y) {
					return Direction.N;
				} else if (x > o.x && y > o.y) {
					return Direction.NE;
				} else if (x > o.x && y == o.y) {
					return Direction.E;
				} else if (x > o.x && y < o.y) {
					return Direction.SE;
				} else if (x == o.x && y < o.y) {
					return Direction.S;
				} else if (x < o.x && y < o.y) {
					return Direction.SW;
				} else if (x < o.x && y == o.y) {
					return Direction.W;
				} else if (x < o.x && y > o.y) {
					return Direction.NW;
				} else {
					throw new IllegalArgumentException("Are in the same position");
				}
			}
		}
	}

	private record Move(Direction direction, int distance) {

		private static final Pattern pattern = Pattern.compile("(?<direction>[UDLR]) (?<distance>[0-9]+)");

		public static Move parse(String representation) {
			Matcher matcher = pattern.matcher(representation);
			if (matcher.matches()) {
				return new Move(
					Direction.parse(matcher.group("direction")),
					Integer.parseInt(matcher.group("distance"))
				);
			} else {
				throw new IllegalArgumentException(representation);
			}
		}
	}

	private enum Direction {
		N, S, W, E, NW, NE, SW, SE;

		static Direction parse(String direction) {
			return switch (direction) {
				case "U" -> N;
				case "D" -> S;
				case "L" -> W;
				case "R" -> E;
				default -> throw new IllegalArgumentException(direction);
			};
		}
	}
}