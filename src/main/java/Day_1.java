import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day_1 {

	public static void main(String[] args) throws IOException, URISyntaxException {
		Stream<String> lines = Files.lines(Path.of(Day_1.class.getResource("Day_1.txt").toURI()));
		int richestThreeElfsCalories = lines
			.map(line -> {
				if (line.isBlank()) {
					return System.lineSeparator();
				} else {
					return line;
				}
			})
			.collect(Collectors.joining(" "))
			.lines().map(String::trim)
			.map(line -> new Elf(Arrays.stream(line.split(" ")).toList()))
			.sorted(Comparator.comparingInt(Elf::allCalories).reversed())
			.limit(3)
			.mapToInt(Elf::allCalories)
			.sum();

		System.out.println(richestThreeElfsCalories);
	}

	record Elf(List<String> items) {

		int allCalories() {
			return items().stream().mapToInt(Integer::parseInt).sum();
		}
	}
}
