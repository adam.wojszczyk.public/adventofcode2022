import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;

public class Day_12 {

	public static void main(String[] args) {
		List<String> lines = Input.lines("Day_12.txt");
		part1(lines);
		part2(lines);
	}

	private static void part1(List<String> lines) {
		Terrain terrain = new Terrain(lines);
		Location start = terrain.start();
		BFSWanderer.wander(start);
		System.out.println(terrain.end().distance());
	}

	private static void part2(List<String> lines) {
		Terrain terrain = new Terrain(lines);
		Set<Integer> shortestPathsFromTheBottom = new HashSet<>();
		Set<Location> start = terrain.lowestLocations();
		for (Location location : start) {
			if(BFSWanderer.wander(location)) {
				shortestPathsFromTheBottom.add(terrain.end().distance());
			}
			terrain.clearMarks();
		}
		System.out.println(shortestPathsFromTheBottom.stream().mapToInt(i -> i).min().orElseThrow());
	}

	private static class BFSWanderer {

		static boolean wander(Location start) {
			Queue<Location> unvisited = new PriorityQueue<>(List.of(start));
			start.proposeDistance(0);
			while (!unvisited.isEmpty()) {
				Location current = unvisited.remove();
				if(current.isEnd()) {
					return true;
				}
				current.markAsVisited();
				List<Location> neighbours = current.getTraversableNotVisitedNeighbours();
				unvisited.addAll(neighbours);
				for (Location neighbour : neighbours) {
					neighbour.proposeDistance(current.distance() + 1);
				}
				unvisited = new PriorityQueue<>(unvisited.stream().distinct().toList());
			}
			return false;
		}
	}

	private static class Terrain {

		List<List<Location>> rows;

		private Terrain(List<String> rows) {
			this.rows = rows.stream()
				.map(row -> Arrays.stream(row.split("")))
				.map(row -> row.map(Location::new).toList())
				.toList();
			connectHorizontally();
			connectVertically();
		}

		private void connectHorizontally() {
			for (List<Location> row : rows) {
				Location previous = null;
				for (Location current : row) {
					if (previous != null) {
						previous.connectTo(Direction.E, current);
					}
					previous = current;
				}
			}
		}

		private void connectVertically() {
			for (List<Location> column : Tables.columns(rows)) {
				Location previous = null;
				for (Location current : column) {
					if (previous != null) {
						previous.connectTo(Direction.S, current);
					}
					previous = current;
				}
			}
		}

		public String visitedMap() {
			String map = "";
			for (List<Location> row : rows) {
				for (Location location : row) {
					if(location.notVisited()) {
						map += StringUtils.leftPad("" + location.elevation(), 3);
					} else {
						map += StringUtils.leftPad("x", 3);
					}
				}
				map += System.lineSeparator();
			}
			return map;
		}

		@Override
		public String toString() {
			String map = "";
			for (List<Location> row : rows) {
				for (Location location : row) {
					map += StringUtils.leftPad("" + location.elevation(), 3);
				}
				map += System.lineSeparator();
			}
			return map;
		}

		public Location start() {
			return rows.stream().flatMap(Collection::stream).filter(Location::isStart).findFirst().orElseThrow();
		}

		public Location end() {
			return rows.stream().flatMap(Collection::stream).filter(Location::isEnd).findFirst().orElseThrow();
		}

		public Set<Location> lowestLocations() {
			return rows.stream().flatMap(Collection::stream).filter(Location::onTheBottom).collect(Collectors.toSet());
		}

		public void clearMarks() {
			rows.stream().flatMap(Collection::stream).forEach(Location::clearMarks);
		}
	}

	private static class Location implements Comparable<Location> {

		private final int elevation;
		private final Map<Direction, Location> neighbours = new HashMap<>();
		private final boolean start;
		private final boolean end;
		private boolean visited = false;
		private int distance = Integer.MAX_VALUE;

		Location(String representation) {
			this(representation.charAt(0));
		}

		private Location(char c) {
			this(elevation(c), isStart(c), isEnd(c));
		}

		private Location(int elevation, boolean start, boolean end) {
			this.elevation = elevation;
			this.start = start;
			this.end = end;
		}

		private static boolean isStart(char c) {
			return c == 'S';
		}

		private static boolean isEnd(char c) {
			return c == 'E';
		}

		private static int elevation(char c) {
			return switch (c) {
				case 'S' -> 0;
				case 'E' -> 'z' - 'a';
				default -> c - 'a';
			};
		}

		void connectTo(Direction direction, Location neighbour) {
			if (!neighbours.containsKey(direction)) {
				neighbours.put(direction, neighbour);
				neighbour.neighbours.put(direction.opposite(), this);
			} else {
				throw new IllegalArgumentException("Already has neighbour assigned to this direction " + direction);
			}
		}

		public List<Location> getTraversableNotVisitedNeighbours() {
			return neighbours.values().stream()
				.filter(Location::notVisited)
				.filter(this::isTraversable)
				.toList();
		}

		boolean isTraversable(Location other) {
			return  other.elevation() <= elevation + 1;
		}

		public void markAsVisited() {
			this.visited = true;
		}

		private boolean notVisited() {
			return !visited;
		}

		public void proposeDistance(int distance) {
			this.distance = Math.min(distance, this.distance);
		}

		public int distance() {
			return distance;
		}

		public int elevation() {
			return elevation;
		}

		public boolean isStart() {
			return start;
		}

		public boolean isEnd() {
			return end;
		}

		@Override
		public int compareTo(Location o) {
			return Integer.compare(distance, o.distance);
		}

		private boolean onTheBottom() {
			return elevation == 0;
		}

		private void clearMarks() {
			this.visited = false;
			this.distance = Integer.MAX_VALUE;
		}
	}

	enum Direction {
		N, S, W, E;

		private Direction opposite;

		static {
			N.opposite = S;
			S.opposite = N;
			W.opposite = E;
			E.opposite = W;
		}

		Direction opposite() {
			return opposite;
		}
	}
}