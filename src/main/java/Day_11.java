import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Day_11 {

	public static void main(String[] args) {
		List<String> lines = Input.lines("Day_11.txt");
		List<String> notes = lines.stream()
			.filter(line -> !line.contains("Monkey"))
			.filter(line -> !line.isBlank())
			.map(String::trim)
			.toList();

		part1(notes);
		part2(notes);
	}

	private static void part1(List<String> notes) {
		List<Monkey> monkeys = parseMonkeys(notes, true);
		Game game = new Game(monkeys);
		for (int i = 0; i < 20; i++) {
			game.round();
		}
		long monkeyBusinessLevel = monkeyBusinessLevel(monkeys);
		System.out.println(monkeyBusinessLevel);
	}

	private static void part2(List<String> notes) {
		List<Monkey> monkeys = parseMonkeys(notes, false);
		Game game = new Game(monkeys);
		for (int i = 0; i < 10000; i++) {
			game.round();
			System.out.println(i + " " + monkeys);
		}
		long monkeyBusinessLevel = monkeyBusinessLevel(monkeys);
		System.out.println(monkeyBusinessLevel);
	}

	private static List<Monkey> parseMonkeys(List<String> notes, boolean relief) {
		List<Monkey> monkeys = new ArrayList<>();
		int monkeysCount = notes.size() / 5;
		for (int monkeyId = 0; monkeyId < monkeysCount; monkeyId++) {
			List<String> description = notes.subList(0 + (monkeyId * 5), 5 + (monkeyId * 5));
			monkeys.add(new Monkey(description, monkeys, relief));
		}
		return monkeys;
	}

	private static long monkeyBusinessLevel(List<Monkey> monkeys) {
		return monkeys.stream()
			.sorted(Comparator.comparingLong(Monkey::inspectedItemsCounter).reversed())
			.limit(2)
			.mapToLong(Monkey::inspectedItemsCounter)
			.reduce((left, right) -> left * right)
			.orElseThrow();
	}

	private static class Game {

		private final List<Monkey> monkeys;

		private Game(List<Monkey> monkeys) {this.monkeys = monkeys;}

		void round() {
			for (Monkey monkey : monkeys) {
				monkey.inspectAndThrow();
			}
		}
	}

	private static class Monkey {

		private final List<Item> items;
		private final Operation operation;
		private final Test test;
		private final Throw throwIfTrue;
		private final Throw throwIfFalse;
		private final boolean relief;
		private final List<Monkey> monkeys;
		private long inspectedItemsCounter = 0;

		private Monkey(List<String> description, List<Monkey> monkeys, boolean relief) {
			this.items = new ArrayList<>(
				Arrays.stream(description.get(0).replaceAll("Starting items: ", "").split(", "))
					.mapToInt(Integer::parseInt)
					.mapToObj(Item::new)
					.toList()
			);
			this.operation = Operation.parse(description.get(1));
			this.test = new Test(description.get(2));
			this.throwIfTrue = new Throw(description.get(3), monkeys);
			this.throwIfFalse = new Throw(description.get(4), monkeys);
			this.relief = relief;
			this.monkeys = monkeys;
		}

		public void accept(Item item) {
			items.add(item);
		}

		public void inspectAndThrow() {
			List<Item> toThrowTrue = new ArrayList<>();
			List<Item> toThrowFalse = new ArrayList<>();
			for (Item item : items) {
				inspectedItemsCounter++;
				long worryLevel = operation.execute(item.worryLevel());
				if(relief) {
					worryLevel = Math.floorDiv(worryLevel, 3);
				} else {
					worryLevel = worryLevel %  monkeys.stream().map(m -> m.test.divisibleBy()).reduce((left, right) -> left * right).orElseThrow();;
				}
				item.setWorryLevel(worryLevel);
				if (test.check(item)) {
					toThrowTrue.add(item);
				} else {
					toThrowFalse.add(item);
				}
			}
			toThrowTrue.forEach(throwIfTrue::perform);
			toThrowFalse.forEach(throwIfFalse::perform);
			items.clear();
		}

		private long inspectedItemsCounter() {
			return inspectedItemsCounter;
		}

		@Override
		public String toString() {
			return String.valueOf(inspectedItemsCounter);
		}
	}

	private static class Throw {

		private final int monkeyId;
		private final List<Monkey> monkeys;

		Throw(String description, List<Monkey> monkeys) {
			this(Integer.parseInt(description.replaceAll("[^0-9]", "")), monkeys);
		}

		private Throw(int monkeyId, List<Monkey> monkeys) {
			this.monkeyId = monkeyId;
			this.monkeys = monkeys;
		}

		void perform(Item item) {
			Monkey monkey = monkeys.get(monkeyId);
			monkey.accept(item);
		}
	}

	private static class Item {

		private long worryLevel;

		Item(long worryLevel) {
			this.worryLevel = worryLevel;
		}

		long worryLevel() {
			return worryLevel;
		}

		void setWorryLevel(long worryLevel) {
			this.worryLevel = worryLevel;
		}

		@Override
		public String toString() {
			return String.valueOf(worryLevel);
		}
	}

	private record Test(long divisibleBy) {

		Test(String description) {
			this(Long.parseLong(description.replaceAll("[^0-9]", "")));
		}

		boolean check(Item item) {
			return item.worryLevel() % divisibleBy == 0;
		}
	}

	private sealed interface Operation permits Add, Multiply, Square {

		long execute(long value);

		static Operation parse(String description) {
			return switch (description) {
				case String d when d.endsWith(" * old") -> new Square();
				case String d when d.contains(" * ") -> new Multiply(value(d));
				case String d when d.contains(" + ") -> new Add(value(d));
				default -> throw new IllegalArgumentException(description);
			};
		}

		private static int value(String description) {
			return Integer.parseInt(description.replaceAll("[^0-9]", ""));
		}
	}

	private record Add(long addend) implements Operation {

		@Override
		public long execute(long value) {
			return value + addend;
		}
	}

	private record Multiply(long multiplicand) implements Operation {

		@Override
		public long execute(long value) {
			return value * multiplicand;
		}
	}

	private static final class Square implements Operation {

		@Override
		public long execute(long value) {
			return value * value;
		}
	}
}