import java.util.ArrayList;
import java.util.List;

public class Tables {

	static <ELEMENT> List<List<ELEMENT>> columns(List<List<ELEMENT>> rows) {
		int columnsCount = rows.get(0).size();
		ArrayList<List<ELEMENT>> columns = new ArrayList<>();
		for (int c = 0; c < columnsCount; c++) {
			List<ELEMENT> column = new ArrayList<>();
			for (List<ELEMENT> row : rows) {
				column.add(row.get(c));
			}
			columns.add(column);
		}
		return columns;
	}
}