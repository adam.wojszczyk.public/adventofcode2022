import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Streams;

import java.util.List;
import java.util.stream.IntStream;

public class Day_13 {

	public static void main(String[] args) {
		part1();
	}

	private static void part1() {
		checkOrder("Day_13_example.txt");
		checkOrder("Day_13.txt");
	}

	private static void checkOrder(String example) {
		List<String> lines = Input.lines(example).stream().filter(l -> !l.isEmpty()).toList();
		List<List<String>> groups = Input.groupLines(2, lines);
		List<Pair> pairs = groups.stream()
			.map(g -> new Pair(g.get(0), g.get(1)))
			.toList();
		int sumOfIndexesOfPairsInOrder = Streams.zip(
				IntStream.iterate(1, p -> p + 1).boxed(),
				pairs.stream().map(Pair::comparison),
				WithIndex::new
			)
			.filter(c -> c.comparison().equals(Comparison.IN_ORDER))
			.mapToInt(WithIndex::index)
			.sum();
		System.out.println(pairs);
		System.out.println(sumOfIndexesOfPairsInOrder);
	}

	private record WithIndex(int index, Comparison comparison) {}

	private record Pair(JsonNode left, JsonNode right) {

		private static final ObjectMapper mapper = new ObjectMapper();

		public Pair(String left, String right) {
			this(parse(left), parse(right));
		}

		private static JsonNode parse(String left) {
			try {
				return mapper.readTree(left);
			} catch (JsonProcessingException e) {
				throw new RuntimeException(e);
			}
		}

		Comparison comparison() {
			if (left.isInt() && right.isInt()) {
				return compareIntegers();
			} else if (left.isArray() && right.isArray()) {
				return compareLists();
			} else if (left.isArray() && right.isInt()) {
				return new Pair(left, mapper.createArrayNode().add(right)).comparison();
			} else if (left.isInt() && right.isArray()) {
				return new Pair(mapper.createArrayNode().add(left), right).comparison();
			} else {
				throw new IllegalStateException();
			}
		}

		private Comparison compareIntegers() {
			if (left.asInt() == right.asInt()) {
				return Comparison.NO_DECISION;
			} else if (left.asInt() < right.asInt()) {
				return Comparison.IN_ORDER;
			} else {
				return Comparison.NOT_IN_ORDER;
			}
		}

		private Comparison compareLists() {
			List<Comparison> comparisons = Streams.zip(
					Streams.stream(left.elements()),
					Streams.stream(right.elements()),
					Pair::new
				)
				.map(Pair::comparison)
				.toList();
			for (Comparison comparison : comparisons) {
				if (Comparison.IN_ORDER.equals(comparison)) {
					return Comparison.IN_ORDER;
				} else if (Comparison.NOT_IN_ORDER.equals(comparison)) {
					return Comparison.NOT_IN_ORDER;
				} else {
					// go on
				}
			}
			if (left.size() < right.size()) {
				return Comparison.IN_ORDER;
			} else if (left.size() > right.size()) {
				return Comparison.NOT_IN_ORDER;
			} else {
				return Comparison.NO_DECISION;
			}
		}
	}

	private enum Comparison {
		IN_ORDER, NOT_IN_ORDER, NO_DECISION
	}
}
