
import com.google.common.collect.Range;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class Day_4 {

	public static void main(String[] args) throws URISyntaxException, IOException {
		List<String> lines = Files.lines(Path.of(Day_4.class.getResource("Day_4.txt").toURI())).toList();

		long oneContainsTheOther = lines.stream()
			.map(Pair::new)
			.filter(Pair::oneContainsTheOther)
			.count();
		System.out.println(oneContainsTheOther);

		long overlap = lines.stream()
			.map(Pair::new)
			.filter(Pair::overlap)
			.count();
		System.out.println(overlap);
	}

	private record Pair(Range<Integer> elf1, Range<Integer> elf2) {

		public Pair(String pair) {
			this(pair.split(","));
		}

		private Pair(String[] pair) {
			this(pair[0], pair[1]);
		}

		private Pair(String elf1, String elf2) {
			this(parse(elf1), parse(elf2));
		}

		private static Range<Integer> parse(String elf) {
			String[] range = elf.split("-");
			return Range.closed(Integer.parseInt(range[0]), Integer.parseInt(range[1]));
		}

		public boolean oneContainsTheOther() {
			return elf1.encloses(elf2) || elf2.encloses(elf1);
		}

		public boolean overlap() {
			return elf1.isConnected(elf2);
		}
	}
}
