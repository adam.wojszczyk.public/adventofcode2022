import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Day_8 {

	public static void main(String[] args) throws URISyntaxException, IOException {
		List<String> lines = Files.lines(Path.of(Day_8.class.getResource("Day_8.txt").toURI())).toList();
		Forest forest = new Forest(lines);

		long visibleTrees = forest.trees().stream()
			.filter(Tree::isVisible)
			.count();
		System.out.println(visibleTrees);

		int maxScenicScore = forest.trees().stream()
			.mapToInt(Tree::scenicScore)
			.max()
			.orElseThrow();
		System.out.println(maxScenicScore);
	}

	private static class Forest {

		private final List<List<Tree>> trees;

		private Forest(List<String> lines) {
			this.trees = lines.stream()
				.map(l -> Arrays.stream(l.split("")).map(Integer::parseInt).map(Tree::new).toList())
				.toList();
			connectInRows();
			connectInColumns();
		}

		private void connectInRows() {
			for (List<Tree> row : trees) {
				Tree left = null;
				for (Tree current : row) {
					current.connectWithLeft(left);
					left = current;
				}
			}
		}

		private void connectInColumns() {
			for (List<Tree> column : columns()) {
				Tree up = null;
				for (Tree current : column) {
					current.connectWithUp(up);
					up = current;
				}
			}
		}

		private List<List<Tree>> columns() {
			int columnsCount = trees.get(0).size();
			ArrayList<List<Tree>> columns = new ArrayList<>();
			for (int c = 0; c < columnsCount; c++) {
				List<Tree> column = new ArrayList<>();
				for (List<Tree> row : trees) {
					column.add(row.get(c));
				}
				columns.add(column);
			}
			return columns;
		}

		Set<Tree> trees() {
			return trees.stream().flatMap(List::stream).collect(Collectors.toSet());
		}
	}

	private static class Tree {

		private final int height;
		private Tree up;
		private Tree down;
		private Tree left;
		private Tree right;

		private Tree(int height) {
			this.height = height;
		}

		public void connectWithLeft(Tree left) {
			if (left != null) {
				this.left = left;
				left.right = this;
			}
		}

		public void connectWithUp(Tree up) {
			if (up != null) {
				this.up = up;
				up.down = this;
			}
		}

		public boolean isVisible() {
			return allLowerInDirection(height, tree -> tree.up) ||
				allLowerInDirection(height, tree -> tree.down) ||
				allLowerInDirection(height, tree -> tree.left) ||
				allLowerInDirection(height, tree -> tree.right);
		}

		private boolean allLowerInDirection(int height, Function<Tree, Tree> direction) {
			if (direction.apply(this) == null) {
				return true;
			} else if (direction.apply(this).height < height && direction.apply(this).allLowerInDirection(height, direction)) {
				return true;
			} else {
				return false;
			}
		}

		public int scenicScore() {
			return viewingDistanceInDirection(height, tree -> tree.up) *
				viewingDistanceInDirection(height, tree -> tree.down) *
				viewingDistanceInDirection(height, tree -> tree.left) *
				viewingDistanceInDirection(height, tree -> tree.right);
		}

		private int viewingDistanceInDirection(int height, Function<Tree, Tree> direction) {
			if (direction.apply(this) == null) {
				return 0;
			} else if (direction.apply(this).height < height) {
				return 1 + direction.apply(this).viewingDistanceInDirection(height, direction);
			} else {
				return 1;
			}
		}
	}
}