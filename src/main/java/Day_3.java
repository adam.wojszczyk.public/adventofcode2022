import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day_3 {

	public static void main(String[] args) throws URISyntaxException, IOException {
		List<String> lines = Files.lines(Path.of(Day_3.class.getResource("Day_3.txt").toURI())).toList();
		List<Rucksack> rucksacks = lines.stream().map(Rucksack::new).toList();

		int sum = rucksacks.stream()
			.mapToInt(r -> r.sharedItems().stream().mapToInt(Item::priority).sum())
			.sum();
		System.out.println(sum);

		int sumInGroup = Lists.partition(rucksacks, 3).stream()
			.map(Group::new)
			.mapToInt(group -> group.sharedItems().stream().mapToInt(Item::priority).sum())
			.sum();
		System.out.println(sumInGroup);
	}

	private record Group(List<Rucksack> rucksacks) {

		Set<Item> sharedItems() {
			return rucksacks().stream()
				.map(Rucksack::allItems)
				.reduce(Sets::intersection)
				.get();
		}
	}

	private record Rucksack(Compartment left, Compartment right) {

		public Rucksack(String rucksack) {
			this(rucksack, rucksack.length() / 2);
		}

		private Rucksack(String rucksack, int indexOfLastElementInALeftCompartment) {
			this(
				new Compartment(rucksack.substring(0, indexOfLastElementInALeftCompartment)),
				new Compartment(rucksack.substring(indexOfLastElementInALeftCompartment))
			);
		}

		public Set<Item> sharedItems() {
			return left.sharedItemsWith(right);
		}

		public Set<Item> allItems() {
			return Stream.of(left.items(), right.items())
				.flatMap(Collection::stream)
				.collect(Collectors.toSet());
		}
	}

	private record Compartment(Set<Item> items) {

		public Compartment(String items) {
			this(Arrays.stream(items.split("")).map(Item::new).collect(Collectors.toSet()));
		}

		Set<Item> sharedItemsWith(Compartment other) {
			return Sets.intersection(this.items, other.items);
		}
	}

	private record Item(String type) {

		private static final String priorities = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

		int priority() {
			return priorities.indexOf(type) + 1;
		}
	}
}
