import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

public class Day_10 {

	public static void main(String[] args) {
		List<String> lines = Input.lines("Day_10.txt");
		List<Instruction> instructions = lines.stream().map(Instruction::parse).toList();
		CPU cpu = new CPU(instructions);
		CRT crt = new CRT(cpu);
		Clock clock = new Clock(cpu, crt);

		List<State> states = new ArrayList<>();
		while(cpu.hasInstructionsToPerform()) {
			states.add(cpu.state());
			clock.tick();
		}

		int sum = states.stream()
			.filter(s -> List.of(20, 60, 100, 140, 180, 220).contains(s.cycleNumber()))
			.mapToInt(State::signalStrength)
			.sum();
		System.out.println(sum);
	}

	private static class CRT {

		private int column = 0;
		private final CPU cpu;

		private CRT(CPU cpu) {this.cpu = cpu;}

		void draw() {
			Sprite sprite = new Sprite(cpu.registerValue());
			if(sprite.inPixel(column)) {
				System.out.print("#");
			} else {
				System.out.print(".");
			}
			if(column == 39) {
				column = 0;
				System.out.println();
			} else {
				column++;
			}
		}
	}

	private record Sprite(int position) {

		boolean inPixel(int pixelPosition) {
			return position == pixelPosition || position == pixelPosition - 1 || position == pixelPosition + 1;
		}
	}

	private static class Clock {

		private final CPU cpu;
		private final CRT crt;

		private Clock(CPU cpu, CRT crt) {
			this.cpu = cpu;
			this.crt = crt;
		}

		void tick() {
			crt.draw();
			cpu.tick();
		}
	}

	private record State(int cycleNumber, int register, int signalStrength) {}

	private static class CPU {

		private final Deque<Cycle> cycles;
		private int cycleNumber = 1;
		private int register = 1;

		public CPU(List<Instruction> instructions) {
			cycles = new ArrayDeque<>(instructions.stream().flatMap(i -> i.cycles().stream()).toList());
		}

		void tick() {
			if (hasInstructionsToPerform()) {
				Cycle cycle = cycles.removeFirst();
				register = cycle.execute(register);
				cycleNumber++;
			} else {
				// idle
			}
		}

		private boolean hasInstructionsToPerform() {
			return !cycles.isEmpty();
		}

		int signalStrength() {
			return cycleNumber * register;
		}

		int registerValue() {
			return register;
		}

		public State state() {
			return new State(cycleNumber, register, signalStrength());
		}
	}

	private sealed interface Instruction permits Noop, Add {

		List<Cycle> cycles();

		static Instruction parse(String representation) {
			if (representation.equals("noop")) {
				return new Noop();
			} else {
				return new Add(representation);
			}
		}
	}

	private static final class Noop implements Instruction {

		@Override
		public List<Cycle> cycles() {
			return List.of(Cycle.EMPTY);
		}
	}

	private static final class Add implements Instruction {

		private final int addend;

		private Add(String representation) {
			this(Integer.parseInt(representation.split(" ")[1]));
		}

		private Add(int addend) {this.addend = addend;}

		@Override
		public List<Cycle> cycles() {
			return List.of(Cycle.EMPTY, value -> value + addend);
		}
	}

	private interface Cycle {

		int execute(int value);

		Cycle EMPTY = value -> value;
	}
}