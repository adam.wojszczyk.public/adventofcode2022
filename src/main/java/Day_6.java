
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Day_6 {

	public static void main(String[] args) throws IOException, URISyntaxException {
		List<String> signal = List.of(
			Files.lines(Path.of(Day_6.class.getResource("Day_6.txt").toURI()))
				.toList()
				.get(0)
				.split("")
		);
		System.out.println(markerIndex(4, signal));
		System.out.println(markerIndex(14, signal));
	}

	private static int markerIndex(int markerSize, List<String> signal) {
		Window window = new Window(markerSize);
		for (int i = 0; i < signal.size(); i++) {
			window.add(signal.get(i));
			if (window.containsPacketMarker()) {
				return i + 1;
			}
		}
		throw new IllegalStateException("No marker was found");
	}

	private static class Window {

		private final List<String> window = new ArrayList<>();
		private final int size;

		private Window(int size) {
			this.size = size;
		}

		public void add(String character) {
			window.add(character);
			if (window.size() == size + 1) {
				window.remove(0);
			}
		}

		boolean containsPacketMarker() {
			return Set.copyOf(window).size() == size;
		}
	}
}
