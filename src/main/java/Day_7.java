import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day_7 {

	public static void main(String[] args) throws URISyntaxException, IOException {
		List<String> lines = Files.lines(Path.of(Day_7.class.getResource("Day_7.txt").toURI())).toList();
		Directory root = new Parser(lines).root();
		Set<Directory> all = Stream
			.concat(Stream.of(root), root.listAllDirectories().stream())
			.collect(Collectors.toSet());

		long sum = all.stream()
			.filter(d -> d.size() <= 100000L)
			.mapToLong(Directory::size)
			.sum();
		System.out.println(root);
		System.out.println(sum);

		long total = 70_000_000L;
		long occupied = root.size();
		long required = 30_000_000;
		long free = total - occupied;
		long toFreeUp = required - free;

		Directory toDelete = all.stream()
			.sorted(Comparator.comparingLong(Directory::size))
			.filter(d -> d.size() >= toFreeUp)
			.findFirst()
			.orElseThrow();
		System.out.println(toDelete);
	}

	private static class Parser {

		private final List<String> lines;
		private final Pattern changeDirectory = Pattern.compile("\\$ cd (?<name>.*)");
		private final Pattern file = Pattern.compile("(?<size>[0-9]*) (?<name>.*)");

		private Parser(List<String> lines) {this.lines = lines;}

		Directory root() {
			Directory root = new Directory("/.");
			Directory current = root;
			for (String line : lines) {
				Matcher changeDirectoryMatcher = changeDirectory.matcher(line);
				Matcher fileMatcher = file.matcher(line);

				if (changeDirectoryMatcher.matches()) {
					String directoryName = changeDirectoryMatcher.group("name");
					if (directoryName.equals("/")) {
						current = root;
					} else if(directoryName.equals("..")) {
						current = current.parent();
					} else {
						current = current.getOrCreateChild(directoryName);
					}
				} else if (fileMatcher.matches()) {
					current.add(new File(fileMatcher.group("name"), Long.parseLong(fileMatcher.group("size"))));
				} else {
					// ignore
				}
			}
			return root;
		}
	}

	private static class Directory {

		private final Set<Directory> children = new HashSet<>();
		private final Set<File> files = new HashSet<>();
		private final String name;
		private final Directory parent;

		private Directory(String name) {
			this(name, null);
		}

		private Directory(String name, Directory parent) {
			this.name = name;
			this.parent = parent;
		}

		boolean isRoot() {
			return parent == null;
		}

		long size() {
			long childrenSize = children.stream().mapToLong(Directory::size).sum();
			long filesSize = files.stream().mapToLong(File::size).sum();
			return childrenSize + filesSize;
		}

		Directory parent() {
			return parent;
		}

		public Directory getOrCreateChild(String directoryName) {
			Directory child = children.stream()
				.filter(d -> directoryName.equals(d.name))
				.findFirst()
				.orElse(new Directory(directoryName, this));
			children.add(child);
			return child;
		}

		public void add(File file) {
			files.add(file);
		}

		public Set<Directory> listAllDirectories() {
			return Stream.concat(
				children.stream(),
				children.stream().flatMap(c -> c.listAllDirectories().stream())
			).collect(Collectors.toSet());
		}

		@Override
		public String toString() {
			return new StringJoiner(", ", Directory.class.getSimpleName() + "[", "]")
				.add("name='" + name + "'")
				.add("size=" + size())
				.toString();
		}
	}

	private record File(String name, long size) {}
}
