import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.List;

public class Day_5 {

	public static void main(String[] args) throws URISyntaxException, IOException {
		List<String> lines = Files.lines(Path.of(Day_5.class.getResource("Day_5.txt").toURI())).toList();

		List<String> stacks = lines.stream()
			.filter(line -> line.contains("["))
			.toList();
		List<Operation> operations = lines.stream()
			.filter(line -> line.contains("move"))
			.map(Operation::new)
			.toList();

		Ship ship = Ship.fromString(stacks);
		ship.rearrangeOneByOne(operations);
		List<Crate> topCrates = ship.pickUpTopCrates();
		topCrates.forEach(System.out::print);

		System.out.print("\n---\n");

		Ship secondShip = Ship.fromString(stacks);
		secondShip.rearrangeInBatches(operations);
		List<Crate> secondShipTopCrates = secondShip.pickUpTopCrates();
		secondShipTopCrates.forEach(System.out::print);
	}

	private static class Ship {

		private final List<Stack> stacks = new ArrayList<>();

		public static Ship fromString(List<String> stacks) {
			Ship ship = new Ship();
			List<String> stacksReversed = new ArrayList<>(stacks);
			Collections.reverse(stacksReversed);
			stacksReversed.stream()
				.map(s -> s.replaceAll(" \\[", "["))
				.map(s -> s.replaceAll("] ", "]"))
				.map(s -> s.replaceAll(" {4}", "   "))
				.map(s -> s.replaceAll(" {3}", " "))
				.map(s -> s.replaceAll("\\[", ""))
				.map(s -> s.replaceAll("]", ""))
				.forEach(ship::loadALayer);
			return ship;
		}

		private void loadALayer(String layer) {
			List<Crate> crates = Arrays.stream(layer.split("")).map(Crate::new).toList();
			prepareStacks(crates);
			putCratesOnStacks(crates);
		}

		private void prepareStacks(List<Crate> crates) {
			int lackOfStacks = Math.max(0, crates.size() - stacks.size());
			if (lackOfStacks > 0) {
				for (int i = 0; i < lackOfStacks; i++) {
					stacks.add(new Stack());
				}
			}
		}

		private void putCratesOnStacks(List<Crate> crates) {
			for (int i = 0; i < crates.size(); i++) {
				if (!crates.get(i).isEmpty()) {
					stacks.get(i).putOn(crates.get(i));
				}
			}
		}

		public void rearrangeOneByOne(List<Operation> operations) {
			operations.forEach(
				operation -> {
					for (int i = 0; i < operation.amount(); i++) {
						Crate crate = stacks.get(operation.from()).pickUp();
						stacks.get(operation.to()).putOn(crate);
					}
				}
			);
		}

		public void rearrangeInBatches(List<Operation> operations) {
			operations.forEach(
				operation -> {
					Batch batch = stacks.get(operation.from()).pickUp(operation.amount());
					stacks.get(operation.to()).putOn(batch);
				}
			);
		}

		public List<Crate> pickUpTopCrates() {
			return stacks.stream().map(Stack::pickUp).toList();
		}
	}

	private static class Stack {

		private final Deque<Crate> stack = new ArrayDeque<>();

		public void putOn(Batch batch) {
			while(batch.hasCrates()) {
				putOn(batch.pop());
			}
		}

		public void putOn(Crate crate) {
			stack.push(crate);
		}

		public Batch pickUp(int howManyInBatch) {
			Batch batch = new Batch();
			for (int i = 0; i < howManyInBatch; i++) {
				batch.push(pickUp());
			}
			return batch;
		}

		public Crate pickUp() {
			return stack.pop();
		}
	}

	private static class Batch {
		private final Deque<Crate> crates = new ArrayDeque<>();

		public void push(Crate crate) {
			crates.push(crate);
		}

		public Crate pop() {
			return crates.pop();
		}

		public boolean hasCrates() {
			return !crates.isEmpty();
		}
	}

	private record Crate(String type) {

		public boolean isEmpty() {
			return type.isBlank();
		}
	}

	private record Operation(int from, int to, int amount) {

		public Operation(String operation) {
			this(operation
				.replaceAll("[a-z]", "")
				.replaceAll(" {2}", " ")
				.trim()
				.split(" ")
			);
		}

		private Operation(String[] operation) {
			this(
				Integer.parseInt(operation[1]) - 1,
				Integer.parseInt(operation[2]) - 1,
				Integer.parseInt(operation[0])
			);
		}
	}
}
